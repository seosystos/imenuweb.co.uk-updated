﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace iMenuWebsite2.AppCode
{
    public static class Email
    {
        public static void SendEmailNoLog(string vFrom, string vTo, string vCc, string vBcc, string vSubject, string vBody, bool bIsHTML)
        {
            var mySmtpHost = "auth.smtp.1and1.co.uk";
            var myPort = 587;
            var mySslStatus = true;
            var myUsername = "info@imenuweb.co.uk";
            var myPassword = "Dynam1cs";
            var myDefaultStatus = false;

            var mailObj = new MailMessage();

            mailObj.From = new MailAddress(vFrom);
            mailObj.To.Add(vTo);

            if (vCc != "")
            {
                mailObj.To.Add(vCc);
            }

            if (vBcc != "")
            {
                mailObj.To.Add(vBcc);
            }

            mailObj.Subject = vSubject;
            mailObj.Body = vBody;
            mailObj.IsBodyHtml = bIsHTML;

            var myAdminEmail = vFrom;

            try
            {
                using (var SMTPClient = new SmtpClient(mySmtpHost))
                {
                    SMTPClient.UseDefaultCredentials = myDefaultStatus;
                    SMTPClient.Host = mySmtpHost;
                    SMTPClient.Port = myPort;
                    SMTPClient.EnableSsl = mySslStatus;
                    SMTPClient.Timeout = 200000;

                    if (SMTPClient.UseDefaultCredentials)
                    {
                        SMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                    }
                    else
                    {
                        SMTPClient.Credentials = new NetworkCredential(myUsername, myPassword);
                    }
                    SMTPClient.Send(mailObj);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static bool IsValidEmail(string vEmail)
        {
            string email = vEmail;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
                return true;
            else
                return false;
        }
    }
}