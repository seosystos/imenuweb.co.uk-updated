﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iMenuWebsite2.AppCode
{
    public static class Format
    {
        public static string NullToString(object vString)
        {
            return vString == null ? "" : vString.ToString();
        }

        public static string NullToString(string vString)
        {
            return vString == null ? "" : vString;
        }
    }
}