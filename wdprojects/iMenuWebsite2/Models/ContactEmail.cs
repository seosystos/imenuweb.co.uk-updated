﻿using System.ComponentModel.DataAnnotations;

namespace iMenuWebsite2.Models
{
    public class ContactEmail
    {
        [Required]
        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string Message { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}