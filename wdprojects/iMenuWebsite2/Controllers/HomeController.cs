﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iMenuWebsite2.AppCode;
using iMenuWebsite2.Models;

namespace iMenuWebsite2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {            
            return View();
        }

        [Route("takeaway-ordering-system")]
        [HttpGet]
        public ActionResult Benefits()
        {
            System.Web.HttpContext.Current.Session["pageName"] = "takeaway-ordering-system";
            return View();
        }

        [HttpGet]
        public ActionResult GetInTouchEmail()
        {
            var model = new ContactEmail();
            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GetInTouchEmail(ContactEmail model)
        {
            var pageName = System.Web.HttpContext.Current.Session["pageName"];
            string myAlert = "";

            bool myEmailIsValid = false;

            myEmailIsValid = Email.IsValidEmail(Format.NullToString(model.Email));

            model.Name = Server.HtmlEncode(Format.NullToString(model.Name));
            model.Email = Server.HtmlEncode(Format.NullToString(model.Email));
            model.PhoneNumber = Server.HtmlEncode(Format.NullToString(model.PhoneNumber));
            model.Message = Server.HtmlEncode(Format.NullToString(model.Message));

            if ((pageName.ToString().ToLower() == "benefits" || pageName.ToString().ToLower() == "contact") & (!myEmailIsValid || model.Message == ""))
            {
                myAlert = Server.HtmlEncode("Your details are incorrect. Please try again. Thank you.");
            }
            else if (pageName.ToString().ToLower() == "features-pricing" & !myEmailIsValid)
            {
                myAlert = Server.HtmlEncode("Your details are incorrect. Please try again. Thank you.");
            }
            else
            {
                string myMessage = $"Name : {model.Name}<br />Email: {model.Email}<br />Phone Number {model.PhoneNumber}<br />Message : {model.Message}";
                Email.SendEmailNoLog("info@imenuweb.co.uk", "info@imenuweb.co.uk", "", "", "iMENUWEB WEBSITE QUERY", myMessage, true);
            }
            
            return Redirect("/" + pageName);
        }

        public ActionResult FeaturesPricingContactMeEmail()
        {
            var model = new ContactEmail();
            return View(model);
        }

        [Route("contact")]
        [HttpGet]
        public ActionResult Contact(string vMessage)
        {
            System.Web.HttpContext.Current.Session["pageName"] = "contact";
            ViewBag.Alert = Server.HtmlDecode(vMessage);
            return View();
        }

        [Route("fast-food-restaurant-website")]
        public ActionResult Features_Pricing()
        {
            System.Web.HttpContext.Current.Session["pageName"] = "fast-food-restaurant-website";
            return View();
        }

        [Route("best-restaurant-websites")]
        public ActionResult How_it_Works()
        {
            return View();
        }

        [Route("take-a-tour")]
        public ActionResult Take_Tour()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

     
    }
}