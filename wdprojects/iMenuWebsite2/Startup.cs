﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(iMenuWebsite2.Startup))]
namespace iMenuWebsite2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
